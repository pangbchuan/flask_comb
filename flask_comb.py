# coding=utf-8
import json

import datetime

from apscheduler.schedulers.blocking import BlockingScheduler
from flask import Flask, render_template, request, Response
from flask_apscheduler import APScheduler
from common.url_path import HELLO_WORLD, FIRST_TEMPLATE, TEMPLATE_ARGS, GET_ARGS_TO_FLASK, POST_ARGS_TO_FLASK, TEST_POST

app = Flask(__name__)

@app.route(HELLO_WORLD)
def hello_world():
    '''第一个get路由返回'''
    return 'Hello World!'

@app.route(FIRST_TEMPLATE)
def first_template():
    '''get方法返回一个html模版'''
    return render_template('test1.html')

@app.route(TEMPLATE_ARGS)
def template_args():
    '''get方法返回一个html模版 且返回参数到网页
    http://127.0.0.1:5000/template_args'''
    my_url = 'www.baidu.com'
    my_list = [1, 2, 3, 4, 5, 6]
    my_dict = {"name": 'pangbchuan', "age": 34}
    my_int = 890
    return render_template('test2.html',
                           url=my_url,
                           list=my_list,
                           dict=my_dict,
                           int=my_int)


@app.route(GET_ARGS_TO_FLASK)
def get_args_to_flask():
    '''接收get方法传入的参数并且打印出来
    http://127.0.0.1:5000/get_args_to_flask?name=pbc&age=28'''
    print(request.method)
    print(request.args.get("name"))
    print(request.args.get("age"))

    retu_str = "方法为：%s <br> name参数为：%s <br> age为：%s" %(request.method,request.args.get("name"),request.args.get("age"))

    return retu_str

@app.route(TEST_POST)
def test_post():
    '''post 参数调试页面
    http://127.0.0.1:5000/test_post'''
    return render_template('test3.html')

@app.route(POST_ARGS_TO_FLASK,methods=['POST'])
def post_args_to_flask():
    '''接收get方法传入的参数并且打印出来
    http://127.0.0.1:5000/post_args_to_flask'''
    print(request.method)
    print(request.form.get('name'))
    print(request.form.get('age'))

    resp_data = {"status":True}
    response = Response(
        response=json.dumps(resp_data),
        status=200,
        mimetype='application/json'
    )
    #返回一个response
    return response

class Config(object):  # 创建配置，用类
    JOBS = [
        {
            'id': 'job2',
            'func': '__main__:job_1',
            'args': (3, 4),
            'trigger': 'interval',
            'seconds': 50,
        }
    ]

def job_1(a, b):  # 一个函数，用来做定时任务的任务。
    print(str(a) + ' ' + str(b))

app.config.from_object(Config())  # 为实例化的flask引入配置
if __name__ == '__main__':
    scheduler = APScheduler()  # 实例化APScheduler
    scheduler.init_app(app)  # 把任务列表放进flask
    scheduler.start()  # 启动任务列表
    app.run(host='0.0.0.0', port=5000, debug=True)
