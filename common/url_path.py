#coding=utf-8

'''
存放路由地址
'''

HELLO_WORLD = '/'
FIRST_TEMPLATE = '/first_template'
TEMPLATE_ARGS = '/template_args'
GET_ARGS_TO_FLASK = '/get_args_to_flask'
TEST_POST = '/test_post'
POST_ARGS_TO_FLASK = '/post_args_to_flask'